package com.example.database_design;

import com.example.database_design.initial.InitialData;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        //SpringApplication.run(new Class[]{MainApplication.class, InitialData.class}, args);
        SpringApplication.run(MainApplication.class, args);
    }
}
