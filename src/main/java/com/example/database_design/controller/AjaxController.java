//package com.example.database_design.controller;
//
//import com.example.database_design.dto.StudentDto;
//import com.example.database_design.entity.*;
//import com.example.database_design.repository.BatchRepository;
//import com.example.database_design.repository.DepartmentRepository;
//import com.example.database_design.repository.SemesterRepository;
//import com.example.database_design.repository.StudentRepository;
//import org.springframework.beans.BeanUtils;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Controller
//@RequestMapping("/students")
//public class AjaxController {
//
//    private final StudentRepository studentRepository;
//    private final DepartmentRepository departmentRepository;
//    private final BatchRepository batchRepository;
//    private final SemesterRepository semesterRepository;
//
//    public AjaxController(StudentRepository studentRepository, DepartmentRepository departmentRepository, BatchRepository batchRepository, SemesterRepository semesterRepository) {
//        this.studentRepository = studentRepository;
//        this.departmentRepository = departmentRepository;
//        this.batchRepository = batchRepository;
//        this.semesterRepository = semesterRepository;
//    }
//
//    @GetMapping("/")
//    public String getAll(Model m) {
//        m.addAttribute("departmentList", departmentRepository.findAll());
//        m.addAttribute("batchList", batchRepository.findAll());
//        m.addAttribute("semesterList", semesterRepository.findAll());
//        return "student/manage-student";
//    }
//
//    @GetMapping(value = "/getAll", consumes = "application/json", produces = "application/json")
//    public @ResponseBody List<StudentDto> getAllStudent_onLoad() {
//        return getAllStudentDtos();
//    }
//
//
//    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public @ResponseBody StudentDto saveStudent(@RequestBody StudentDto studentDto) {
//        Department department = departmentRepository.getOne(studentDto.getDepId());
//        Batch batch = batchRepository.getOne(studentDto.getBatchId());
//
//        Student student = new Student();
//        BeanUtils.copyProperties(studentDto, student);
//        student.setDepartment(department);
//        student.setBatch(batch);
//        studentRepository.save(student);
//
//        studentDto.setDepName(department.getDepartmentName());
//        StudentDto studentDto1 = new StudentDto();
//        BeanUtils.copyProperties(student, studentDto1);
//        studentDto1.setDepName(department.getDepartmentName());
//        studentDto1.setBatchName(batch.getBatchName());
//        return studentDto1;
//    }
//
//
//    @GetMapping(value = "/getOne/{id}", consumes = "application/json", produces = "application/json")
//    public @ResponseBody StudentDto getOne(@PathVariable("id") Long id) {
//        Student studentTemp = studentRepository.findById(id).get();
//        StudentDto studentDtoTemp = new StudentDto();
//
//        BeanUtils.copyProperties(studentTemp, studentDtoTemp);
//        studentDtoTemp.setDepId(studentTemp.getDepartment().getDepId());
//        studentDtoTemp.setDepName(studentTemp.getDepartment().getDepartmentName());
//
//        studentDtoTemp.setBatchId(studentTemp.getBatch().getBatchId());
//        studentDtoTemp.setBatchName(studentTemp.getBatch().getBatchName());
//        return studentDtoTemp;
//    }
//
//    @PostMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public @ResponseBody List<StudentDto> update(@RequestBody StudentDto studentDto) {
//
//        Student student = studentRepository.findById(studentDto.getStuId()).get();
//
//        student.setDepartment(departmentRepository.getOne(studentDto.getDepId()));
//        student.setBatch(batchRepository.getOne(studentDto.getBatchId()));
//        student.setStudentName(studentDto.getStudentName());
//        studentRepository.save(student);
//
//        return getAllStudentDtos();
//    }
//
//    @PostMapping(value = "/delete/{id1}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public @ResponseBody List<StudentDto> delete(@PathVariable("id1") Long id) {
//        studentRepository.deleteById(id);
//        return getAllStudentDtos();
//    }
//
//    @GetMapping(value = "/getCourses/{id}", consumes = "application/json", produces = "application/json")
//    public @ResponseBody List<Course> getCourse(@PathVariable("id") Long id) {
//        Semester semesterTemp = semesterRepository.findById(id).get();
//        return semesterTemp.getCourses();
//    }
//
//    private List<StudentDto> getAllStudentDtos() {
//        List<Student> studentList = studentRepository.findAll();
//        List<StudentDto> studentDtoList = new ArrayList<>();
//
//        for (Student s : studentList) {
//            StudentDto sTemp = new StudentDto();
//            BeanUtils.copyProperties(s, sTemp);
//            sTemp.setDepName(s.getDepartment().getDepartmentName());
//            sTemp.setBatchName(s.getBatch().getBatchName());
//            studentDtoList.add(sTemp);
//        }
//        return studentDtoList;
//    }
//}
