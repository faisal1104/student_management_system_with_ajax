package com.example.database_design.controller;

import com.example.database_design.dto.StudentDto;
import com.example.database_design.entity.*;
import com.example.database_design.repository.*;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentRestConroller {

    private final StudentRepository studentRepository;
    private final DepartmentRepository departmentRepository;
    private final BatchRepository batchRepository;
    private final SemesterRepository semesterRepository;

    public StudentRestConroller(StudentRepository studentRepository, DepartmentRepository departmentRepository, BatchRepository batchRepository, SemesterRepository semesterRepository) {
        this.studentRepository = studentRepository;
        this.departmentRepository = departmentRepository;
        this.batchRepository = batchRepository;
        this.semesterRepository = semesterRepository;
    }

    @GetMapping(value = "/getAll")
    public List<StudentDto> getAllStudent_onLoad() {
        return getAllStudentDtos();
    }


    @PostMapping(value = "/save")
    public StudentDto saveStudent(@Validated @RequestBody StudentDto studentDto) {
        Department department = departmentRepository.getOne(studentDto.getDepId());
        Batch batch = batchRepository.getOne(studentDto.getBatchId());
        Semester semester = semesterRepository.getOne(studentDto.getSemesterId());

        Student student = new Student();
        BeanUtils.copyProperties(studentDto, student);
        student.setDepartment(department);
        student.setBatch(batch);
        student.setSemester(semester);
        studentRepository.save(student);

        StudentDto studentDto1 = new StudentDto();
        BeanUtils.copyProperties(student, studentDto1);
        studentDto1.setDepName(department.getDepartmentName());
        studentDto1.setBatchName(batch.getBatchName());
        studentDto1.setSemesterName(semester.getSemesterName());

        List<Course> courseListTemp1 = student.getSemester().getCourses();
        List<String> courseNameTemmp;
        courseNameTemmp = new ArrayList<>();
        for (Course c: courseListTemp1) {
            courseNameTemmp.add(c.getCourseName());
        }
        studentDto1.setCourseNames(courseNameTemmp);
        return studentDto1;
    }


    @GetMapping(value = "/getOne/{id}")
    public StudentDto getOne(@PathVariable("id") Long id) {

        Student studentTemp = studentRepository.findById(id).get();
        StudentDto studentDtoTemp = new StudentDto();

        BeanUtils.copyProperties(studentTemp, studentDtoTemp);
        studentDtoTemp.setDepId(studentTemp.getDepartment().getDepId());
        studentDtoTemp.setDepName(studentTemp.getDepartment().getDepartmentName());
        studentDtoTemp.setSemesterId(studentTemp.getSemester().getSemesterId());
        studentDtoTemp.setSemesterName(studentTemp.getSemester().getSemesterName());

        studentDtoTemp.setBatchId(studentTemp.getBatch().getBatchId());
        studentDtoTemp.setBatchName(studentTemp.getBatch().getBatchName());
        return studentDtoTemp;
    }

    @PostMapping(value = "/update")
    public List<StudentDto> update(@RequestBody StudentDto studentDto) {

        Student student = studentRepository.findById(studentDto.getStuId()).get();

        student.setDepartment(departmentRepository.getOne(studentDto.getDepId()));
        student.setBatch(batchRepository.getOne(studentDto.getBatchId()));
        student.setStudentName(studentDto.getStudentName());
        studentRepository.save(student);

        return getAllStudentDtos();
    }

    @PostMapping(value = "/delete/{id1}")
    public List<StudentDto> delete(@PathVariable("id1") Long id) {
        studentRepository.deleteById(id);
        return getAllStudentDtos();
    }

    @GetMapping(value = "/getSemesters/{id3}")
    public List<Semester> getSemesters(@PathVariable("id3") Long id1) {
        Department department = departmentRepository.findById(id1).get();
        return department.getSemesters();
    }

    @GetMapping(value = "/getCourses/{id}")
    public List<Course> getCourse(@PathVariable("id") Long id) {
        Semester semesterTemp = semesterRepository.findById(id).get();
        return semesterTemp.getCourses();
    }


    private List<StudentDto> getAllStudentDtos() {
        List<Student> studentList = studentRepository.findAll();
        List<StudentDto> studentDtoList = new ArrayList<>();
        List<Course> courseListTemp;
        List<String> courseNameTemmp;
        for (Student s : studentList) {
            courseListTemp = s.getSemester().getCourses();

            StudentDto sTemp = new StudentDto();
            BeanUtils.copyProperties(s, sTemp);
            sTemp.setDepName(s.getDepartment().getDepartmentName());
            sTemp.setBatchName(s.getBatch().getBatchName());
            sTemp.setSemesterName(s.getSemester().getSemesterName());

            courseNameTemmp = new ArrayList<>();
            for (Course c: courseListTemp) {
                courseNameTemmp.add(c.getCourseName());
            }
            sTemp.setCourseNames(courseNameTemmp);
            studentDtoList.add(sTemp);
        }
        return studentDtoList;
    }
}

