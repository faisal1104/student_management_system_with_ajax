package com.example.database_design.controller;

import com.example.database_design.entity.Student;
import com.example.database_design.repository.BatchRepository;
import com.example.database_design.repository.DepartmentRepository;
import com.example.database_design.repository.StudentRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/stu")
public class StudentController {

    private final StudentRepository studentRepository;
    private final DepartmentRepository departmentRepository;
    private final BatchRepository batchRepository;

    public StudentController(StudentRepository studentRepository, DepartmentRepository departmentRepository, BatchRepository batchRepository) {
        this.studentRepository = studentRepository;
        this.departmentRepository = departmentRepository;
        this.batchRepository = batchRepository;
    }

    @GetMapping("/")
    public String getAll(Model m) {
        List<Student> students = studentRepository.findAll();
        m.addAttribute("students", students);
        return "student/view";
    }

    @GetMapping("/add")
    public String add(Model m) {

        m.addAttribute("student", new Student());
        m.addAttribute("departmentList", departmentRepository.findAll());
        m.addAttribute("batchList", batchRepository.findAll());
        return "student/add";
    }

    @PostMapping("/save")
    public RedirectView add(@ModelAttribute("student") Student student) {
        System.out.println(student.getStudentName());
        studentRepository.save(student);
        return new RedirectView("/student/");
    }
}
